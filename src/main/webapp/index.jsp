<%-- 
    Document   : index
    Created on : 01-04-2020, 18:04:42
    Author     : cathe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 01 - Aplicaciones empresariales</title>
        <style>
body {
  background-image: url("images/fondo3.jpg");
}
</style>    
    </head>
    
        <body>
          
            <h1 style="color:white">"Calculadora de Interes"</h1>
         
        <form name="form" action="controller" method="POST">
           <table>
            <tr>
              <th style="color:white">Monto:</th>
              <th><input type="text" name="monto" required class="form-control" /></th>
            </tr>
            <tr>
              <th style="color:white">Tasa:</th>
              <td><input type="text" step="any" name="tasa" required class="form-control" /></td>
            </tr>
            <tr>
              <th style="color:white">Años:</th>
              <td><input type="text" name="anos" required class="form-control" /></td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Calcular" /></br></td>
            </tr>
          </table>
        </form>
        </div
    </body>
</html>

